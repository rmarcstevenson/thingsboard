# Author - Riley Stevenson
import requests
import random
import time
import sys

# main function to gather the temperature and humidity data and send to
# thingsboard every 10 seconds
# no inputs
# no return value
def ThingsBoardPOST():
    input("Welcome! Press 'Enter' to start!")
    try:
        print("Sending telemetry data to ThingsBoard")
        while(True):
            # generate some data to be used
            data = generateData()

            # host that will connect to the thingsboard instance
            host = "http://iot2.rivercityinnovations.ca:8080/api/v1/integrations/http/416b6563-bb74-b796-bcfb-d81f88fe3ea9"
            payload = {"temperature":data[0], "humidity":data[1]} # payload to be sent
            
            requests.post(host, payload)    # send the post request to the host along with the payload
            time.sleep(10)  # sleep for 10 seconds 
    except KeyboardInterrupt:
        print("\nGoodbye!")
        sys.exit()
        

# helper function to generate some temperature and humidity data
# no inputs
# returns tuple with the temperature and humidity data
def generateData():

    # generate random temperature and humidity data (rounded to 2 decimal places)
    temperature = round(random.uniform(-45, 35), 2)
    humidity = round(random.uniform(0, 100), 2)
    
    return (temperature, humidity)

# run function
ThingsBoardPOST()
