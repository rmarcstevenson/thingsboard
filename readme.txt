Python Script written by Riley Stevenson

Instructions for running this Python Script:

This script is completely automated and will continue running until user sends a keyboard interrupt.

To start the script follow these instructions:
    1. Ensure the "thingsboard_script.py" file is downloaded to your current directory
    2. Run the following command to start the script: "python3 thingsboard_script.py"
    3. The script will prompt you to press the enter key to start the program
    4. The script will then begin running and sending telemetry data to ThingsBoard.
    5. To exit the script, simply press "ctrl+c" to trigger a keyboard interrupt. 

Specfics:
    This python script randomly generates temperature data between -45 and 35 degrees and humidity data
    between 0 and 100 % both rounded to 2 decimal places
    This data is then sent every 10 seconds to ThingsBoard and displayed on "My Dashboard"

Enjoy!

- Riley S.